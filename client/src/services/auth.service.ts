import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    redirectToAuth() {
        window.location.href = "https://auth.gog.com/auth?client_id=51205051179528065&redirect_uri=https://localhost/&response_type=code"
    }
}