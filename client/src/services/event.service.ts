import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {map} from 'rxjs/operators';
import { GogGameResponse } from './game.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private httpClient: HttpClient) { }

  createEvent(event?: GamingEvent): Observable<any> {
    // const eventToAdd: GamingEvent = {
    //   categories: ['CAT1', 'CAT2'],
    //   gameId: '1207664663',
    //   startDate: '2018-06-01T11:31:25.000Z',
    //   endDate: '2018-06-01T17:31:25.000Z',
    //   createdBy: 'Endrju Golara',
    //   description: 'NIEZWYKLE PIĘKNE GRANIE OJEJ',
    //   participants: ['Andrzej Gąska', 'Antek Sałata']
    // };
    return this.httpClient.post('http://localhost:3000/api/v1/events/create', event).pipe(map(result => {
      return result;
    }));
  }

  getAllEvents() {
    return this.httpClient.get<EventsResponse>('http://localhost:3000/api/v1/events/all');
  }

  getEventsForGivenGame(gameId: number) {
    return this.httpClient.get<EventsResponse>('http://localhost:3000/api/v1/events/game?gameId='+gameId);
  }
}

export interface EventsResponse {
  myEvents: GamingEvent[],
  allEvents: GamingEvent[]
}

export interface GamingEvent {
  participants?: string[];
  categories: string[];
  createdBy: string;
  startDate: string;
  endDate: string;
  gameId: string;
  description: string;
  gamersNumber: number;
  tags: string[];
}
