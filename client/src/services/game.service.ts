import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {map} from "rxjs/operators";

export interface Game {
  id: string;
  name: string;
  thumbUrl: string;
  isMultiplayer?: boolean;
}



@Injectable({
  providedIn: 'root'
})
export class GameService {
  constructor(private httpClient: HttpClient) {}

  getAll(): Observable<GogGameResponse> {
    return this.httpClient.get<any>("https://api.gog.com/v1/games").pipe(map(result => {
      return result._embedded.items.map(game => game._embedded.product);
    }));
  }

  getDetails(id: string): Observable<Product> {
    return this.httpClient.get<GogGameResponse>('https://api.gog.com/v1/games/' + id).pipe(map(result => {
      return result._embedded.product;
    }));
  }

  getThumbnailUri(game: GogGameResponse) {
    // console.log("URI", game._links.image.href.replace("{formatter}", "product_630"))
    return game._links.image.href.replace('{formatter}', 'product_630').toString();
  }
}

export interface GogGameResponse {
  description:   string;
  copyrights:    string;
  inDevelopment: InDevelopment;
  _links:        GogGameResponseLinks;
  _embedded:     GogGameResponseEmbedded;
}

export interface GogGameResponseEmbedded {
  product:                   Product;
  tags:                      Publisher[];
  localizations:             Localization[];
  videos:                    Video[];
  screenshots:               Screenshot[];
  bonuses:                   Publisher[];
  developers:                Publisher[];
  publisher:                 Publisher;
  supportedOperatingSystems: SupportedOperatingSystem[];
  features:                  Publisher[];
  esrbRating:                EsrbRating;
  pegiRating:                PegiRating;
}

export interface Publisher {
  name: string;
}

export interface EsrbRating {
  category:           Publisher;
  contentDescriptors: ContentDescriptor[];
}

export interface ContentDescriptor {
  descriptor: string;
}

export interface Localization {
  _embedded: LocalizationEmbedded;
}

export interface LocalizationEmbedded {
  language:          Language;
  localizationScope: LocalizationScope;
}

export interface Language {
  code: string;
  name: string;
}

export interface LocalizationScope {
  type: string;
}

export interface PegiRating {
  ageRating:          number;
  contentDescriptors: ContentDescriptor[];
}

export interface Product {
  id:                 number;
  title:              string;
  isInstallable:      boolean;
  isAvailableForSale: boolean;
  isVisibleInCatalog: boolean;
  isPreorder:         boolean;
  globalReleaseDate:  string;
  _links:             ProductLinks;
}

export interface ProductLinks {
  checkout: Store;
  prices:   Prices;
  image:    Screenshot;
}

export interface Store {
  href: string;
}

export interface Screenshot {
  href:       string;
  templated:  boolean;
  formatters: string[];
}

export interface Prices {
  href:      string;
  templated: boolean;
}

export interface SupportedOperatingSystem {
  operatingSystem:    Publisher;
  systemRequirements: string;
}

export interface Video {
  provider: string;
  _links:   VideoLinks;
}

export interface VideoLinks {
  self:      Store;
  thumbnail: Store;
}

export interface GogGameResponseLinks {
  self:                  Store;
  isRequiredByGames:     Store[];
  requiresGames:         Store[];
  isIncludedInGames:     Store[];
  includesGames:         Store[];
  backgroundImage:       Store;
  galaxyBackgroundImage: Store;
  icon:                  Store;
  store:                 Store;
  forum:                 Store;
  support:               Store;
  image:                 Store;
}

export interface InDevelopment {
  active: boolean;
  until:  string;
}
