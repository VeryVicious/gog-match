import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { mergeMap } from 'rxjs/operators';
import { GameService, Game, GogGameResponse } from '../../services/game.service';
import { of } from 'rxjs';
import { TagsPillsComponent } from '../tags-pills/tags-pills.component';
import { GamingEvent, EventService } from 'src/services/event.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnInit {
  private gameId: string;
  private gameData: GogGameResponse;
  private isLoading = true;

  private gamersNumber: number;
  private description: string;
  private isDiscussion: boolean;
  private isStreaming: boolean;
  private isMulti: boolean;
  private startDate: Date
  private endDate: Date

  @ViewChild(TagsPillsComponent) tags: TagsPillsComponent;

  constructor(
    private route: ActivatedRoute,
    private gameService: GameService,
    private location: Location,
    private eventService: EventService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.pipe(mergeMap(params => {
      if (!params['id']) { return of(null); }
      this.gameId = params['id'];
      return this.gameService.getDetails(params['id']);
    })).subscribe(game => {
      this.gameData = game;
      this.isLoading = false;
    });
  }

  onSubmit() {
    let categories = new Array<string>();
    if(this.isDiscussion) categories.push("Discussion")
    if(this.isStreaming) categories.push("Steaming")
    if(this.isMulti) categories.push("Multiplayer")
    let event: GamingEvent = {
      categories: categories,
      startDate: this.startDate.toISOString(),
      endDate: this.endDate.toISOString(),
      createdBy: "User Userini",
      description: this.description,
      gameId: this.gameId,
      participants: ["User Userini"],
      gamersNumber: this.gamersNumber,
      tags:this.tags.tags
    }

    return this.eventService.createEvent(event).subscribe(success => {
      this.router.navigate(['/events'])
    })
  }

  getBack() {
    this.location.back();
  }

}
