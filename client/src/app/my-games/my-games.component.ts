import { Component, OnInit } from '@angular/core';
import { GameService, Game, GogGameResponse } from '../../services/game.service';
import { Observable } from 'rxjs';
import { AuthService } from 'src/services/auth.service';
import { EventService } from 'src/services/event.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-my-games',
  templateUrl: './my-games.component.html',
  styleUrls: ['./my-games.component.scss']
})
export class MyGamesComponent implements OnInit {
  private userGames: GogGameResponse;

  constructor(
    private gameService: GameService,
    private eventService: EventService
  ) { }

  ngOnInit() {
    this.gameService.getAll().subscribe(x => { this.userGames = x; });
  }

  createEvent() {
    // return this.gameService.getDetails("1207664663").subscribe(gameDetails => {
    //   console.log(gameDetails)
    // })
    // return this.eventService.createEvent().subscribe();
  }

  getAllGames() {
    // return this.gameService.getAll().subscribe(result => {
    //   console.log("GOT ALL GAMES")
    // })
  }

}
