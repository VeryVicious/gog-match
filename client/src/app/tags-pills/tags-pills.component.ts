import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tags-pills',
  templateUrl: './tags-pills.component.html',
  styleUrls: ['./tags-pills.component.scss']
})
export class TagsPillsComponent implements OnInit {
  private newTag: string;
  public tags: string[] = [];

  constructor() { }

  ngOnInit() {
  }

  onSubmit(event: Event) {
    event.preventDefault();

    if (!this.newTag || this.tags.includes(this.newTag)) { return; }
    this.tags.push(this.newTag);
  }

  onPillClick(tag: string) {
    const idx = this.tags.indexOf(tag);
    if (!isNaN(idx)) {
      this.tags.splice(idx, 1);
      this.newTag = null;
    }
  }

}
