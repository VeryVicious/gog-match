import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagsPillsComponent } from './tags-pills.component';

describe('TagsPillsComponent', () => {
  let component: TagsPillsComponent;
  let fixture: ComponentFixture<TagsPillsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagsPillsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsPillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
