import { Component, OnInit } from '@angular/core';
import { EventService, GamingEvent, EventsResponse } from '../../services/event.service';
import { GameService, GogGameResponse } from '../../services/game.service';
import { ActivatedRoute } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.scss']
})
export class MyEventsComponent implements OnInit {
  private events: EventsResponse;
  private gameId: number;
  private gameData: GogGameResponse;

  constructor(private eventService: EventService, private gameService: GameService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.pipe(mergeMap(params => {
      if (!params['id']) { return of(null); }
      this.gameId = params['id'];
      return this.gameService.getDetails(params['id']);
    })).pipe(mergeMap(game => {
      this.gameData = game;
      return this.eventService.getEventsForGivenGame(this.gameId);
    })).subscribe(events => {
      this.events = events;
    });
    // this.eventService.getAllEvents().subscribe(x => {
    //   this.events = x;
    // });
  }

}
