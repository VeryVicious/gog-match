import { Route } from '@angular/router';
import { PageLayoutComponent } from './page-layout/page-layout.component';
import { MyGamesComponent } from './my-games/my-games.component';
import { AddEventComponent } from './add-event/add-event.component';
import { MyEventsComponent } from './my-events/my-events.component';

export const AppRoutes: Route[] = [
  { path: '', component: PageLayoutComponent, children: [
    { path: '', redirectTo: 'my-games', pathMatch: 'full' },
    { path: 'my-games', component: MyGamesComponent, children: [
      { path: 'add/:id', component: AddEventComponent }
    ]},
    { path: 'events', redirectTo: 'my-games' },
    { path: 'events/:id', component: MyEventsComponent }
  ]}
];
