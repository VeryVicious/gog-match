var express = require('express');
var app = express();
var logger = require('morgan')
var bodyParser = require('body-parser');

var routes = require('./routes')
var mongoose = require('mongoose');
var config = require('./config')
app.use(logger('dev'))
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  var nocache = require('nocache')
app.use(nocache())

mongoose.connect('mongodb://localhost/GOGMatchDB');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 3000;

app.use(config.apiVersion, routes.authRoutes)
app.use(config.apiVersion, routes.userRoutes)
app.use(config.apiVersion, routes.gamingEvents)

app.listen(port);
console.log('Listening on port  ' + port);
