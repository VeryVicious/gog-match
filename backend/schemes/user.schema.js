'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  clientId: {
    type: String,
    required: true,
    unique: true
  }
}, {timestamps: true});

module.exports = mongoose.model('User', UserSchema);
