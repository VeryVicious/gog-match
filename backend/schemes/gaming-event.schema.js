'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GamingEventSchema = new Schema({
 createdBy: {
    type: String,
 },
 participants: [
     {
         type: String
     }
 ],
 startDate: {
     type: Date,
     required: false
 },
 endDate: {
    type: Date,
    required: false
},
gameId: {
    type: String,
    required: true
},
categories: [
    {
        type: String
    }
],
description: {
    type: String,
},
tags: [
    {
        type: String
    }
],
gamersNumber: {
    type: Number
}

}, {timestamps: true});

module.exports = mongoose.model('GamingEvent', GamingEventSchema);
