var jwt = require('jsonwebtoken')
var config = require('../config')

var createToken = function (auth) {
    return jwt.sign({
        id: auth.id
    }, config.jwtSecret, {
        expiresIn: "14d"
    });
};

exports.generateToken = function (req, res, next) {
    
    try {
        req.token = createToken(req.auth);
    }
    catch(error) {
        console.log("ERR: ",error)
    }
    next();
};

exports.sendToken = function (req, res) {
    res.setHeader('Authorization','Bearer '+ req.token);
    res
        .status(200)
        .json({ token: req.token});
};
