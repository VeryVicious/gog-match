var jwt = require('jsonwebtoken')
var secret = require('../config').jwtSecret

exports.auth = (req, res, next) => {
    const authorizationHeader = req.headers['authorization']
    if (!authorizationHeader) {
        res.status(401).json({message: 'User is not logged in.',statusCode: 20})
        return
    }

    let token = authorizationHeader.split(' ')[1]
    const dateNow = new Date().valueOf()
    jwt.verify(token, secret, (err, decoded) => {
        if (err || dateNow.toString().slice(0, 10) > decoded.exp) {
            res.status(401).json({message: 'User is not logged in.',statusCode: 20})
            return
        }
        req.userId = decoded.id
        next()
    })
}