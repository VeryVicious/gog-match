var express = require('express');
var router = express.Router();
var authController = require('../controllers/auth.controller')
var tokenHelper = require('../utils/tokenHelper')
var routeGuard = require('../utils/routeGuard')


router.post('/auth/token', authController.login, tokenHelper.generateToken, tokenHelper.sendToken)
router.post('/auth/refresh', routeGuard.auth, authController.refreshLogin, tokenHelper.generateToken, tokenHelper.sendToken)

module.exports = router;