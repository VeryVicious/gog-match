var express = require('express');
var router = express.Router();
var tokenHelper = require('../utils/tokenHelper')
var routeGuard = require('../utils/routeGuard')
let gamingEventController = require('../controllers/gaming-event.controller')


router.post('/events/create', gamingEventController.createGamingEvent)
router.get('/events/all', gamingEventController.getAllEvents)
router.get('/events/game', gamingEventController.getEventsForGivenGame)
// router.post('/auth/refresh', routeGuard.auth, authController.refreshLogin, tokenHelper.generateToken, tokenHelper.sendToken)

module.exports = router;