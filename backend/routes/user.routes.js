
var express = require('express');
var router = express.Router();
var tokenHelper = require('../utils/tokenHelper')
var routeGuard = require('../utils/routeGuard')
var userController = require('../controllers/user.controller')


// router.get('/user/:userId/profile', routeGuard.auth, userController.getUserById)
router.post('/user/register', userController.createUser)
router.get('/user/profile',routeGuard.auth, userController.getLoggedUser)
// router.post('/user/addFriend', routeGuard.auth, friendsController.addFriend)

module.exports= router;