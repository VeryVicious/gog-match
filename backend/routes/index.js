const userRoutes = require('./user.routes')
const authRoutes = require('./auth.routes')
const gamingEvents = require('./gaming-events.routes')

module.exports = {
    userRoutes,
    authRoutes,
    gamingEvents
}