var mongoose = require('mongoose'),
  GamingEvent = require('../schemes/gaming-event.schema');


exports.createGamingEvent = (req, res) => {
    const newEvent = new GamingEvent({
        createdBy: req.body.createdBy,
        participants: req.body.participants,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        gameId: req.body.gameId,
        categories: req.body.categories,
        description: req.body.description,
        tags: req.body.tags,
        gamersNumber: req.body.gamersNumber
    })

    return GamingEvent
    .create(newEvent)
    .then(event => res.json({message: "Succesfully added an event.", event: event }))
    .catch(err => res.status(400).json({err, message: "Validation error.", statusCode: 21}))
}

exports.getAllEvents = (req, res) => {
    return GamingEvent.find().then(events => {
        let myEvents = events.filter(event => {return event.createdBy == "User Userini"})
        myEvents.forEach(myEvent => {
            events.splice(events.indexOf(myEvent), 1)
        });

        return res.json({myEvents: myEvents, allEvents: events})
    })
}

exports.getEventsForGivenGame = (req, res) => {
    return GamingEvent.find({gameId: req.query.gameId}).then(events => {
        let myEvents = events.filter(event => {return event.createdBy == "User Userini"})
        myEvents.forEach(myEvent => {
            events.splice(events.indexOf(myEvent), 1)
        });

        return res.json({myEvents: myEvents, allEvents: events})
    })
}
