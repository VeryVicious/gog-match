var mongoose = require('mongoose'),
  User = require('../schemes/user.schema');


exports.login = (req, res, next) => {
    User
      .findOne({username: req.body.username})
      .then(user => {
        if (!user) {
          return Promise.reject({message: "User not found.", statusCode: 21})
        } else if (user) {
          return Promise.resolve()
            .then(auth => {
                req.auth = {
                  id: user._id
                }
                return Promise
                  .resolve()
                  .then(next)
            })
        }
      })
      .catch(err => {
        res
          .status(401)
          .json(err)
          .send()
      })
  }

  exports.refreshLogin = (req, res, next) => {
    req.auth = {
      id: req.userId
    }
    next()
  }