var mongoose = require('mongoose'),
    User = require('../schemes/user.schema');
  const ObjectId = mongoose.Types.ObjectId

exports.getUserById = (req, res) => {
  return User
    .findOne({_id: req.params.userId})
    .then(user => {
      let userRes = this.createUserResponse(user)
      res.json(userRes)
    })
    .catch(err => res.status(400).json({message: err, statusCode: 21}))
}

exports.getLoggedUser = (req, res) => {
  return User
  .findOne({_id: req.userId})
  .then(user => {
    let userRes = this.createUserResponse(user)
    res.json(userRes)
  })
  .catch(err => res.status(400).json({message: err, statusCode: 21}))
}


exports.createUser = (req, res) => {
  if (!req.body.username || !req.body.clientId) {
    res
      .status(400)
      .json({message: "You must provide username and GOG client ID", statusCode: 21})
    return
  }

  const newUser = new User({
    username: req.body.username,
    clientId: req.body.clientId,
  });

  User
    .create(newUser)
    .then(user => res.json("Registration success."))
    .catch(err => res.status(400).json({err, message: "Validation error.", statusCode: 21}))
}

exports.createUserResponse = (user) => {
    let userRes = user.toObject();
    delete userRes.__v;
    return userRes
  
  }